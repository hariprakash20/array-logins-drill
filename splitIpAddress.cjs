const users = require('./2-arrays-logins.cjs');

let splitIPs = []
users.forEach(user=>{
    splitIPs.push(ipAddressToArray(user.ip_address));
})

console.log(splitIPs);

function ipAddressToArray(ipAddress){
    let isValidIp = true;
    let ipArray = ipAddress.split('.').reduce((numArray,part)=>{
        if(isValidIp){
        let numPart = Number(part);
        if(numPart >=0 && numPart<=256)
            numArray.push(numPart)
        else{
            isValidIp = false;
            numArray = [];
        }
        }
        return numArray
    },[])
    return ipArray;
}
