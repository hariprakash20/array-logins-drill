const users = require('./2-arrays-logins.cjs');

let countOfOrgAuComEmails = users.reduce((count, user)=>{
    if(user.email.includes(".org") || user.email.includes(".au") || user.email.includes(".com"))
    count++;
    return count;
},0)

console.log(countOfOrgAuComEmails);
