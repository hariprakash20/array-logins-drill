const users = require('./2-arrays-logins.cjs');

let sumOf2ndComponent = 0;
users.forEach(user=>{
    sumOf2ndComponent  += ipAddressToArray(user.ip_address)[1];
})

console.log(sumOf2ndComponent);

function ipAddressToArray(ipAddress){
    let isValidIp = true;
    let ipArray = ipAddress.split('.').reduce((numArray,part)=>{
        if(isValidIp){
        let numPart = Number(part);
        if(numPart >=0 && numPart<=256)
            numArray.push(numPart)
        else{
            isValidIp = false;
            numArray = [];
        }
        }
        return numArray
    },[])
    return ipArray;
}
